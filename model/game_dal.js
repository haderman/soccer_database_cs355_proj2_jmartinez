var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT g.match_id, t.team_name AS HomeTeam, '
    + 'w.team_name AS AwayTeam, g.week, g.winner, g.scoreH, g.scoreA, r.first_name, r.last_name '
    + 'FROM game g '
    + 'LEFT JOIN match_ m ON m.match_id = g.match_id '
    + 'LEFT JOIN referee r ON m.ref_id = r.ref_id '
    + 'LEFT JOIN team t ON t.team_id = g.teamH_id '
    + 'LEFT JOIN team w ON w.team_id = g.teamA_id '
    + 'ORDER BY g.week, g.match_id';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function (match_id, callback) {
    var query = 'SELECT g.match_id, t.team_name AS HomeTeam, ' +
        'w.team_name AS AwayTeam, g.week, g.winner, g.scoreH, g.scoreA, r.first_name, r.last_name ' +
        'FROM game g ' +
        'LEFT JOIN match_ m ON m.match_id = g.match_id ' +
        'LEFT JOIN referee r ON m.ref_id = r.ref_id ' +
        'LEFT JOIN team t ON t.team_id = g.teamH_id ' +
        'LEFT JOIN team w ON w.team_id = g.teamA_id ' +
        'WHERE g.match_id = ?';

    var queryData = [match_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.update = function (params, callback) {
  var query = 'UPDATE game SET winner = ?, scoreH = ?, scoreA = ? WHERE match_id = ?';
  var queryData = [params.winner, params.scoreH, params.scoreA, params.match_id];

  connection.query(query, queryData, function (err, result) {
     if(err) {
         res.send(err);
     }
     else {
         var query = 'SELECT t.team_name AS HomeTeam, ' +
             'w.team_name AS AwayTeam ' +
             'FROM game g ' +
             'LEFT JOIN team t ON t.team_id = g.teamH_id ' +
             'LEFT JOIN team w ON w.team_id = g.teamA_id ' +
             'WHERE g.match_id = ?';
         var queryData = [params.match_id];

         connection.query(query, queryData, function (err, result) {
             if (err) {
                 res.send(err);
             }
             else {
                 callback(err, result[0].HomeTeam, result[0].AwayTeam);
             }

         });
     }
  });
};


exports.insert = function(params, callback) {

    var query = 'INSERT INTO match_ (ref_id) VALUES (?)';
    var queryData = [params.ref_id];

    connection.query(query, queryData, function(err, result) {

        if (err) {
            res.send(err);
        }
        else {
            var match_id = result.insertId;

            var query = 'INSERT INTO game (match_id, teamH_id, teamA_id, week) '
                + 'VALUES (?,?,?,?)';

            var queryData = [match_id, params.home_team_id, params.away_team_id, params.week];

            connection.query(query, queryData, function (err, result) {

                if (err) {
                    res.send(err);
                }
                else {
                    var query = 'SELECT t.team_name AS HomeTeam, ' +
                        'w.team_name AS AwayTeam ' +
                        'FROM game g ' +
                        'LEFT JOIN team t ON t.team_id = g.teamH_id ' +
                        'LEFT JOIN team w ON w.team_id = g.teamA_id ' +
                        'WHERE g.match_id = ?';
                    var queryData = [match_id];

                    connection.query(query, queryData, function (err, result) {
                       if (err) {
                           res.send(err);
                       }
                       else {
                           callback(err, result[0].HomeTeam, result[0].AwayTeam);
                       }

                    });

                }
            });
        }
    });
};

exports.delete = function (match_id, callback) {
    var query = 'DELETE FROM game WHERE match_id = ?';
    var queryData = [match_id];

    connection.query(query, queryData, function (err, result) {
        if (err) {
            res.send(err);
        }
        else {
            var query = 'DELETE from match_ WHERE match_id = ?';
            var queryData = [match_id];
            connection.query(query,queryData, function (err,result) {
               if (err) {
                   res.send(err);
               }
               else {
                   callback(err, result);
               }
            });
        }

    });
};

exports.biased = function (callback) {
  var query = 'SELECT r.first_name AS ref_firstN, r.last_name AS ref_lastN, ma.first_name, ma.last_name, m.match_id, ' +
      'g.winner AS result, g.scoreH as HomeScore, g.scoreA as AwayScore, t.team_name, c.country_name ' +
      'FROM referee r ' +
      'LEFT JOIN match_ m ON m.ref_id = r.ref_id ' +
      'LEFT JOIN game g ON g.match_id = m.match_id ' +
      'LEFT JOIN team t ON g.teamH_id = t.team_id ' +
      'LEFT JOIN manager ma ON t.manager_id = ma.manager_id ' +
      'LEFT JOIN country c ON c.country_id = ma.country_id ' +
      'WHERE ma.country_id = r.country_id';

  connection.query(query, function(err, result) {
      callback(err, result);
  });
};

exports.getNoGames = function (callback) {
  var query = 'SELECT t.team_id, t.team_name FROM team t ' +
      'WHERE NOT EXISTS ( SELECT g.match_id FROM game g ' +
      'WHERE g.teamH_id = t.team_id OR g.teamA_id = t.team_id)';

  connection.query(query, function(err, result) {
      callback(err, result);
  });
};