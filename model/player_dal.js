var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT p.*, t.team_name FROM player p ' +
    'LEFT JOIN team t ON t.team_id = p.team_id ' +
    'ORDER BY first_name, last_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function (player_id, callback) {

    var query = 'CALL player_getinfo(?)';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function (params, callback) {

    var query = 'INSERT INTO player (first_name, last_name, height, position, level_, skills, shooting, '
    + 'passing, pace, defending, physical, dribbling, age, team_id, country_id) '
    + 'VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

    var queryData = [params.first_name, params.last_name, params.height, params.position, params.level_, params.skills,
    params.shooting, params.passing, params.pace, params.defending, params.physical, params.dribbling, params.age, params.team_id, params.country_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, params.first_name, params.last_name);
    });
};

exports.update = function (params, callback) {

    var query = 'UPDATE player SET first_name = ?, last_name = ?, height = ?, position = ?, level_ = ?, skills = ?, '
    + 'shooting = ?, passing = ?, pace = ?, defending = ?, physical = ?, dribbling = ?, age = ?, team_id = ?, country_id = ? '
    + 'WHERE player_id = ?';

    var queryData = [params.first_name, params.last_name, params.height, params.position, params.level_, params.skills,
    params.shooting, params.passing, params.pace, params.defending, params.physical, params.dribbling, params.age, params.team_id,
    params.country_id, params.player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, params.first_name, params.last_name);
    });
};

exports.delete = function (player_id, callback) {

    var query = 'DELETE FROM player WHERE player_id = ?';
    var queryData = [player_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
exports.helpfulShoot = function (params, callback) {
    var query = 'SELECT first_name, last_name, t.team_name, shooting AS stat, player_id FROM player ' +
        'LEFT JOIN team t ON player.team_id = t.team_id ' +
        'WHERE player.shooting > (SELECT MAX(shooting) FROM player ' +
        'LEFT JOIN team n ON player.team_id = n.team_id ' +
        'WHERE n.team_id = ? AND player.position != \'Goalkeeper\') ' +
        'AND player.position != \'Goalkeeper\' ' +
        'ORDER BY shooting DESC';

    var queryData = [params.team_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.helpfulPassing = function (params, callback) {
    var query = 'SELECT first_name, last_name, t.team_name, passing AS stat, player_id FROM player ' +
        'LEFT JOIN team t ON player.team_id = t.team_id ' +
        'WHERE player.passing > (SELECT MAX(passing) FROM player ' +
        'LEFT JOIN team n ON player.team_id = n.team_id ' +
        'WHERE n.team_id = ? AND player.position != \'Goalkeeper\') ' +
        'AND player.position != \'Goalkeeper\' ' +
        'ORDER BY passing DESC';

    var queryData = [params.team_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.helpfulPace = function (params, callback) {
    var query = 'SELECT first_name, last_name, t.team_name, pace AS stat, player_id FROM player ' +
        'LEFT JOIN team t ON player.team_id = t.team_id ' +
        'WHERE player.pace > (SELECT MAX(pace) FROM player ' +
        'LEFT JOIN team n ON player.team_id = n.team_id ' +
        'WHERE n.team_id = ? AND player.position != \'Goalkeeper\') ' +
        'AND player.position != \'Goalkeeper\' ' +
        'ORDER BY pace DESC';

    var queryData = [params.team_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.helpfulDefending = function (params, callback) {
    var query = 'SELECT first_name, last_name, t.team_name, defending AS stat, player_id FROM player ' +
        'LEFT JOIN team t ON player.team_id = t.team_id ' +
        'WHERE player.defending > (SELECT MAX(defending) FROM player ' +
        'LEFT JOIN team n ON player.team_id = n.team_id ' +
        'WHERE n.team_id = ? AND player.position != \'Goalkeeper\') ' +
        'AND player.position != \'Goalkeeper\' ' +
        'ORDER BY defending DESC';

    var queryData = [params.team_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.helpfulPhysical = function (params, callback) {
    var query = 'SELECT first_name, last_name, t.team_name, physical AS stat, player_id FROM player ' +
        'LEFT JOIN team t ON player.team_id = t.team_id ' +
        'WHERE player.physical > (SELECT MAX(physical) FROM player ' +
        'LEFT JOIN team n ON player.team_id = n.team_id ' +
        'WHERE n.team_id = ? AND player.position != \'Goalkeeper\') ' +
        'AND player.position != \'Goalkeeper\' ' +
        'ORDER BY physical DESC';

    var queryData = [params.team_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.helpfulDribbling = function (params, callback) {
    var query = 'SELECT first_name, last_name, t.team_name, dribbling AS stat, player_id FROM player ' +
        'LEFT JOIN team t ON player.team_id = t.team_id ' +
        'WHERE player.dribbling > (SELECT MAX(dribbling) FROM player ' +
        'LEFT JOIN team n ON player.team_id = n.team_id ' +
        'WHERE n.team_id = ? AND player.position != \'Goalkeeper\') ' +
        'AND player.position != \'Goalkeeper\' ' +
        'ORDER BY dribbling DESC';

    var queryData = [params.team_id];
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};