var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM country';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function (country_id, callback) {
    var query = 'SELECT * FROM country WHERE country_id = ?';

    var queryData = [country_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, result);
    });
};

exports.update = function (params, callback) {
    var query = 'UPDATE country SET country_name = ? '
        + 'WHERE country_id = ?';
    var queryData = [params.country_name, params.country_id];

    connection.query(query, queryData, function (err, result) {
        callback(err, params.country_name);
    });
};


exports.insert = function (country_name, callback) {
  var query = 'INSERT INTO country (country_name) VALUES (?)';
  var queryData = [country_name];

  connection.query(query, queryData, function(err, result) {
        callback(err, country_name);
    });
};

exports.delete = function (country_id, callback) {

    var query = 'DELETE FROM country WHERE country_id = ?';
    var queryData = [country_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.getNumPlayers = function (callback) {
  var query = 'SELECT country_name, COUNT(player_id) AS num_players FROM country c ' +
      'LEFT JOIN player p ON p.country_id = c.country_id ' +
      'WHERE p.country_id = c.country_id ' +
      'GROUP BY country_name ' +
      'ORDER BY num_players DESC, country_name';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};