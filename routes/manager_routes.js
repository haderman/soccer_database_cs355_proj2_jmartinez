var express = require('express');
var router = express.Router();

var manager_dal = require('../model/manager_dal');
var country_dal = require('../model/country_dal');

// View All managers
router.get('/all', function(req, res) {
    manager_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('manager/managerViewAll', { 'result':result });
        }
    });

});

router.get('/', function (req, res) {
    if (req.query.manager_id == null) {
        res.send('manager_id is null');
    }
    else {
        manager_dal.getById(req.query.manager_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('manager/managerViewById', {'manager': result[0]});
            }
        });
    }
});

router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    country_dal.getAll(function (err, countries) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('manager/managerAdd', {
                'countries': countries
            });
        }
    });
});

router.post('/insert', function(req, res) { //Changed req.query to req.body !
    // simple validation
    if (req.body.first_name == null || req.body.last_name == null || req.body.country_id == null || req.body.age == null) {
        res.send('First name, last name, age, and country must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        manager_dal.insert(req.body, function (err, managerFName, managerLName) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                manager_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('manager/managerViewAll', {
                            'result': result,
                            'managerF': managerFName,
                            'managerL': managerLName,
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.manager_id == null) {
        res.send('A manager id is required');
    }
    else {
        manager_dal.getById(req.query.manager_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                country_dal.getAll(function (err, countries) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('manager/managerUpdate', {
                            'manager':result[0],
                            'countries': countries
                        });
                    }
                });
            }
        });
    }
});

router.get('/update', function (req,res) {
    manager_dal.update(req.query, function(err, managerFName, managerLName){
        if (err) {
            res.send(err);
        }
        else {
            manager_dal.getAll(function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('manager/managerViewAll', {
                        'result': result,
                        'managerF': managerFName,
                        'managerL': managerLName,
                        'edit_successful': true
                    });
                }
            });
        }
    });
});

// Delete a player for the given player_id
router.get('/delete', function(req, res){
    if(req.query.manager_id == null) {
        res.send('manager id is null');
    }
    else {
        manager_dal.delete(req.query.manager_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                // change to say success?
                res.redirect(302, '/manager/all');
            }
        });
    }
});

module.exports = router;
