var express = require('express');
var router = express.Router();

var stadium_dal = require('../model/stadium_dal');

// View All stadiums
router.get('/all', function(req, res) {
    stadium_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('stadium/stadiumViewAll', { 'result':result });
        }
    });

});

router.get('/', function (req, res) {
    if (req.query.stadium_id == null) {
        res.send('stadium id is null');
    }
    else {
        stadium_dal.getById(req.query.stadium_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('stadium/stadiumViewById', {'stadium': result});
            }
        });
    }
});

router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    stadium_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('stadium/stadiumAdd', {'stadium': result});
        }
    });
});

router.post('/insert', function(req, res) { //Changed req.query to req.body !
    // simple validation
    if (req.body.stadium_name == null || req.body.address == null) {
        res.send('Stadium name and address must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        stadium_dal.insert(req.body, function (err, stadiumName) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                stadium_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('stadium/stadiumViewAll', {
                            'result': result,
                            'stadiumName': stadiumName,
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.stadium_id == null) {
        res.send('A stadium id is required');
    }
    else {
        stadium_dal.getById(req.query.stadium_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('stadium/stadiumUpdate', {
                    'stadium':result[0]
                });
            }
        });
    }
});

router.get('/update', function (req,res) {
    stadium_dal.update(req.query, function(err, stadiumName){
        if (err) {
            res.send(err);
        }
        else {
            stadium_dal.getAll(function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('stadium/stadiumViewAll', {
                        'result': result,
                        'stadiumName': stadiumName,
                        'edit_successful': true
                    });
                }
            });
        }
    });
});

router.get('/delete', function (req, res) {
    if(req.query.stadium_id == null) {
        res.send('stadium id is null');
    }
    else {
        stadium_dal.delete(req.query.stadium_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                // change to say success?
                res.redirect(302, '/stadium/all');
            }
        });
    }
});

module.exports = router;