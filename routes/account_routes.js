var express = require('express');
var router = express.Router();

var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');

// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the account for the given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getById(req.query.account_id, function(err,result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('account/accountViewById', {'result': result[0], 'skills': result[1],
                    'companies': result[2], 'schools': result[3]});
            }
        });
    }
});

// Return the add a new account form
router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    skill_dal.getAll(function (err, result) {
        if (err) {
            res.send(err);
        }
        else {

            company_dal.getAll(function (err, compResult) {
                if (err) {
                    res.send(err);
                }

                else {

                    school_dal.getAll(function (err, schResult) {

                        if (err) {
                            res.send(err);
                        }

                        else {
                            res.render('account/accountAdd', {
                                'skills': result,
                                'companies': compResult,
                                'schools': schResult
                            });
                        }
                    });
                }
            });
        }
    });
});

// View the company for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.first_name == null) {
        res.send('First name must be provided.');
    }
    else if (req.query.last_name == null) {
        res.send('Last name must be provided.')
    }
    else if (req.query.email == null) {
        res.send('An email must be provided.')
    }
    else if(req.query.skill_id == null || req.query.company_id == null || req.query.school_id == null) {
        res.send('At least one skill, school, and company must be selected');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

// Delete an account for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

module.exports = router;