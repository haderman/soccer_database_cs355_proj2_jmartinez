var express = require('express');
var router = express.Router();

var player_dal = require('../model/player_dal');
var team_dal = require('../model/team_dal');
var country_dal = require('../model/country_dal');

// View All players
router.get('/all', function(req, res) {
    player_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('player/playerViewAll', { 'result':result });
        }
    });

});

// View the player for the given player_id
router.get('/', function (req, res) {
   if (req.query.player_id == null) {
       res.send('player_id is null');
   }
   else {
       player_dal.getById(req.query.player_id, function(err, result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('player/playerViewById', {'player': result[0][0], 'country': result[1][0], 'team': result[2][0]});
           }
       });
   }
});

router.get('/add', function(req, res) {
    // passing all the query parameters (req.query) to the insert function instead of each individually
    country_dal.getAll(function (err, countries) {
        if (err) {
            res.send(err);
        }
        else {

            team_dal.getAll(function (err, teams) {
                if (err) {
                    res.send(err);
                }

                else {

                    res.render('player/playerAdd', {
                        'countries': countries,
                        'teams': teams
                    });
                }
            });
        }
    });
});

router.post('/insert', function(req, res) { //Changed req.query to req.body !
    // simple validation
    if (req.body.first_name == null || req.body.last_name == null || req.body.team_id == null || req.body.country_id == null) {
        res.send('First name, last name, team, and country must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        player_dal.insert(req.body, function (err, playerFName, playerLName) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                player_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('player/playerViewAll', {
                            'result': result,
                            'playerF': playerFName,
                            'playerL': playerLName,
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});

router.get('/edit', function(req, res) {
    if (req.query.player_id == null) {
        res.send('A player id is required');
    }
    else {

        player_dal.getById(req.query.player_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                country_dal.getAll(function (err, countries) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        team_dal.getAll(function (err, teams) {
                            if (err) {
                                res.send(err);
                            }
                            else {
                                res.render('player/playerUpdate', {
                                    'player':result[0][0],
                                    'countries': countries,
                                    'teams': teams
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});

router.get('/update', function (req,res) {
    player_dal.update(req.query, function(err, playerFName, playerLName){
        if (err) {
            res.send(err);
        }
        else {

            player_dal.getAll(function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('player/playerViewAll', {
                        'result': result,
                        'playerF': playerFName,
                        'playerL': playerLName,
                        'edit_successful': true
                    });
                }
            });
        }
    });
});

// Delete a player for the given player_id
router.get('/delete', function(req, res){
    if(req.query.player_id == null) {
        res.send('player_id is null');
    }
    else {
        player_dal.delete(req.query.player_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                // change to say success?
                res.redirect(302, '/player/all');
            }
        });
    }
});

router.get('/helpWin', function (req,res) {
   team_dal.getHavePlayers(function (err,result) {
       if (err){
           res.send(err);
       }
       else {
           res.render('player/pickStatTeam', {'teams': result});
       }
   });
});

router.get('/helpful', function (req, res) {
    if (req.query.stat == 1) { //1 shooting, 2 passing, 3 pace, 4 defending, 5 physical, 6 dribbling
        player_dal.helpfulShoot(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/viewHelpful', {'players': result, 'stat': "Shooting"});
            }
        });
    }
    else if (req.query.stat == 2) {
        player_dal.helpfulPassing(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/viewHelpful', {'players': result, 'stat': "Passing"});
            }
        });
    }

    else if (req.query.stat == 3) {
        player_dal.helpfulPace(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/viewHelpful', {'players': result, 'stat': "Pace"});
            }
        });
    }
    else if (req.query.stat == 4) {
        player_dal.helpfulDefending(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/viewHelpful', {'players': result, 'stat': "Defending"});
            }
        });
    }
    else if (req.query.stat == 5) {
        player_dal.helpfulPhysical(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/viewHelpful', {'players': result, 'stat': "Physical"});
            }
        });
    }
    else if (req.query.stat == 6) {
        player_dal.helpfulDribbling(req.query, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('player/viewHelpful', {'players': result,'stat': "Dribbling"});
            }
        });
    }
});

module.exports = router;