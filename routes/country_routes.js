var express = require('express');
var router = express.Router();

var country_dal = require('../model/country_dal');

// View All countries
router.get('/all', function(req, res) {
    country_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('country/countryViewAll', { 'result':result });
        }
    });
});

router.get('/add', function (req, res) {
   res.render('country/countryAdd');
});
/////////////////////////////////////////
router.get('/edit', function(req, res) {
    if (req.query.country_id == null) {
        res.send('A country id is required');
    }
    else {
        country_dal.getById(req.query.country_id, function(err, result) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('country/countryUpdate', {
                    'country':result[0]
                });
            }
        });
    }
});
//////////////////////////////////////////
router.get('/update', function (req,res) {
    country_dal.update(req.query, function(err, countryName){
        if (err) {
            res.send(err);
        }
        else {
            country_dal.getAll(function (err, result) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('country/countryViewAll', {
                        'result': result,
                        'countryName': countryName,
                        'edit_successful': true
                    });
                }
            });
        }
    });
});

router.post('/insert', function (req, res) {
    if (req.body.country_name == null) {
        res.send('Country name and address must be provided.');
    }
    else {
        country_dal.insert(req.body.country_name, function (err, countryName) {
            if (err) {
                console.log(err);
                res.send(err);
            }
            else {
                country_dal.getAll(function (err, result) {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.render('country/countryViewAll', {
                            'result': result,
                            'countryName': countryName,
                            'was_successful': true
                        });
                    }
                });
            }
        });
    }
});

router.get('/delete', function (req, res) {
    if(req.query.country_id == null) {
        res.send('country id is null');
    }
    else {
        country_dal.delete(req.query.country_id, function (err, result) {
            if (err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                // change to say success?
                res.redirect(302, '/country/all');
            }
        });
    }
});

router.get('/numPlayers', function (req, res) {
    country_dal.getNumPlayers(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('country/viewNumPlayers', { 'result':result });
        }
    });
});

module.exports = router;
